extends Node2D

signal player_is_ready(player)
signal street_end()
signal police_radar()
signal caught_speeding()
signal car_collision()

var player_speed: float = 25.0
var player_entity: Entity
var player_day: int = 1
var player_delivery: int = 1
var player_rank: int = 1
var player_failed: int = 0
var player_money: int = 0
var player_xp: int = 0

var time_bonus: int = 0
var money_earned: int = 0
var top_speed: int = 0
var xp_earned: int = 0

var driving_data = {}
var address_data = []
var crime_bucket = []
var traffic_bucket = []
var priority_bucket = []
var summons_types = ['civ', 'cri', 'adm']
var civil_data = ['Contract Breach', 'Injunction', 'Money Claim', 'Property']
var criminal_data = ['Trespassing', 'Armed Robbery', 'Assault', 'Murder']
var admin_data = ['Taxes', 'Immigration', 'Labour', 'Professional']

func set_driving_data(data):
	driving_data = data
	
func _ready():
	
	Game.load_sounds($Sounds)
	Game.load_music($Music)
	
	var addressfile = FileAccess.open("res://address.txt", FileAccess.READ)
	
	while ! addressfile.eof_reached():
		var line = addressfile.get_line()
		address_data.append(line)
		
