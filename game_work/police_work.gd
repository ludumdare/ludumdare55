extends Node2D

@export var police_entity: PackedScene

func _ready():
	Global.police_radar.connect(_on_police_radar)
	Global.caught_speeding.connect(_on_caught_speeding)
	
func _on_police_radar():
	print("radar")
	var police = police_entity.instantiate()
	call_deferred("add_child", police)
	police.position = Global.player_entity.position + Vector2.LEFT * 1000


func _on_caught_speeding():
	print("caught!")
