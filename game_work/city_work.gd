extends Node2D

@export var game_day: int = 1
@export var game_delivery: int = 4
@export var test: bool = false
@export var failed: int = 1

func _ready():
	
	if test:
		
		for i in range(game_day):
			Global.player_day = i
			$City._update_buckets()
		
		Global.player_delivery = game_delivery
		Global.player_day = game_day
		Global.player_failed = failed
			
	Game.start_game()
	Game.set_state(Game.GameStates.PLAY)

