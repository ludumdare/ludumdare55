extends Node2D

@export var distance: int = 5 
@export var setting: String = "cit"
@export var priority: String = "m"
@export var crime: String = "m"
@export var traffic: String = "m"
@export var type: String = "civ"

func _ready():
	
	print(Global.driving_data)
	
	Global.set_driving_data({
		"distance": distance, # 5+
		"setting": setting, # cit, urb
		"priority": priority, #h/m/l
		"crime": crime, # h/m/l
		"traffic": traffic, # h/m/l
		"type": type # civ, crim, adm
	})
	
	print(Global.driving_data)

	Game.start_game()
	Game.set_state(Game.GameStates.PLAY)
