extends Node2D

@export var streets: Array[PackedScene] = []
@onready var root = $StreetRoot

var pos: int = 1

func _ready():
	Global.street_end.connect(_on_street_end)

func _process(delta):
	
	ECS.update()	

func _on_street_end():
	print("end of the street")
	var street = streets[0].instantiate()
	root.call_deferred("add_child", street)
	street.position = Vector2i.RIGHT * (pos * 512)
	pos += 1
	
