extends Component
class_name MoveComponent

@export var speed: float = 10.0
@export var speed_factor: float = 5.0
