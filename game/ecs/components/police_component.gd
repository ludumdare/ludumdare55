extends Component
class_name PoliceComponent

@export var min_speed = 500
@export var max_speed = 1500

var player_entity: Entity
var speed: int = 1000
