extends Component
class_name InputComponent

@export var vertical_speed: float = 5.0
@export var vertical_factor: float = 1.0
