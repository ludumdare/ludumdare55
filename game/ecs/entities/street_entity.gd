extends Area2D

var hit: bool

func _on_body_entered(body):
	
	if not hit:
		Global.street_end.emit()
		hit = true
