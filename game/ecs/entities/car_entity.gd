extends Entity

@export var textures: Array[Texture2D] = []


func on_ready():
	
	var index = randi_range(0, textures.size()-1)
	$Sprite2D.texture = textures[index]
	$CollisionShape2D.shape.radius = 2
	var twidth = textures[index].get_width()
	$CollisionShape2D.shape.height = twidth * 0.8
