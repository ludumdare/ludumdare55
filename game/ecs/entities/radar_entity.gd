extends Area2D

var hit: bool = false

func _on_body_entered(body):
	if not hit:
		Global.police_radar.emit()
		hit = true
