extends System
class_name PoliceSystem

func on_process_entity(entity, delta):
	
	var police = entity.get_component("policecomponent") as PoliceComponent
	
	if entity.global_position.x < Global.player_entity.global_position.x - 50:
		police.speed += 75

	if entity.global_position.x > Global.player_entity.global_position.x + 50:
		police.speed -= 55

	police.speed = clamp(police.speed, police.min_speed, police.max_speed)
	
	var speed = police.speed
	
	entity.velocity = Vector2.RIGHT * speed
	var ypos = lerp(entity.global_position.y, Global.player_entity.global_position.y, 0.005)
	entity.position.y = ypos

	var has_collided = entity.move_and_slide()
	
	if has_collided:
		for i in entity.get_slide_collision_count():
			var collision = entity.get_slide_collision(i)
			var collider = collision.get_collider()
			#print(collision)
			#print(collider.name)
			if collider.name.to_lower() == 'playerentity':
				Global.caught_speeding.emit()
