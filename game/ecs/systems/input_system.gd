extends System
class_name InputSystem


func on_process_entity(entity, delta):
	
	var input = entity.get_component("inputcomponent") as InputComponent
	var dir = entity.get_component("directioncomponent") as DirectionComponent
	
	var gspeed = clamp(Global.player_speed, 50,100)
	
	var speed = input.vertical_speed * input.vertical_factor * delta * gspeed
	
	speed = clamp(speed, 10, 40)
	
	if Input.is_action_pressed("game_up"):
		entity.position += Vector2.UP * speed

	if Input.is_action_pressed("game_down"):
		entity.position += Vector2.DOWN * speed
		
	if Input.is_action_pressed("game_gas"):
		Global.player_speed += 5
		
	if Input.is_action_pressed("game_break"):
		Global.player_speed -= 10
		

	entity.position.y = clamp(entity.position.y, 240, 480)
	Global.player_speed = clamp(Global.player_speed, 50, 400)
	
