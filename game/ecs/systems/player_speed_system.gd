extends System
class_name PlayerSpeedSystem

func on_process_entity(entity, delta):
	var move = entity.get_component("movecomponent") as MoveComponent
	move.speed = Global.player_speed
