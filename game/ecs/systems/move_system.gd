extends System
class_name MoveSystem

func on_process_entity(entity, delta):
	var direction = entity.get_component("directioncomponent") as DirectionComponent
	var move = entity.get_component("movecomponent") as MoveComponent
	entity.position += (direction.direction * move.speed * move.speed_factor * delta)
	
