extends System
class_name  CameraSystem

func on_process_entity(entity, delta):
	
	var camera = entity as Camera2D
	camera.position = Vector2i.ZERO + (Vector2i.RIGHT * 100)
