extends System
class_name CarSystem

func on_process_entity(entity, delta):
	
	var car = entity.get_component("carcomponent") as CarComponent
	entity.velocity = Vector2.RIGHT * car.speed
	var has_collided = entity.move_and_slide()

	if has_collided:
		for i in entity.get_slide_collision_count():
			var collision = entity.get_slide_collision(i)
			var collider = collision.get_collider()
			#print(collision)
			#print(collider.name)
			if collider.name.to_lower() == 'playerentity':
				Global.car_collision.emit()
