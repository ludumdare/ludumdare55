extends Node2D

@export var blocks: Array[PackedScene] = []

@onready var address1 = $UI/Control/NinePatchRect/NinePatchRect/Address1
@onready var address2 = $UI/Control/NinePatchRect/NinePatchRect2/Address2
@onready var address3 = $UI/Control/NinePatchRect/NinePatchRect3/Address3

@onready var miles1 = $UI/Control/NinePatchRect/NinePatchRect/Miles1
@onready var miles2 = $UI/Control/NinePatchRect/NinePatchRect2/Miles2
@onready var miles3 = $UI/Control/NinePatchRect/NinePatchRect3/Miles3

@onready var d1cr1 = $UI/Control/NinePatchRect/NinePatchRect/D1CR1
@onready var d1cr2 = $UI/Control/NinePatchRect/NinePatchRect/D1CR2
@onready var d1cr3 = $UI/Control/NinePatchRect/NinePatchRect/D1CR3
@onready var d1tr1 = $UI/Control/NinePatchRect/NinePatchRect/D1TR1
@onready var d1tr2 = $UI/Control/NinePatchRect/NinePatchRect/D1TR2
@onready var d1tr3 = $UI/Control/NinePatchRect/NinePatchRect/D1TR3
@onready var d1pr1 = $UI/Control/NinePatchRect/NinePatchRect/D1PR1
@onready var d1pr2 = $UI/Control/NinePatchRect/NinePatchRect/D1PR2
@onready var d1pr3 = $UI/Control/NinePatchRect/NinePatchRect/D1PR3

@onready var d2cr1 = $UI/Control/NinePatchRect/NinePatchRect2/D2CR1
@onready var d2cr2 = $UI/Control/NinePatchRect/NinePatchRect2/D2CR2
@onready var d2cr3 = $UI/Control/NinePatchRect/NinePatchRect2/D2CR3
@onready var d2tr1 = $UI/Control/NinePatchRect/NinePatchRect2/D2TR1
@onready var d2tr2 = $UI/Control/NinePatchRect/NinePatchRect2/D2TR2
@onready var d2tr3 = $UI/Control/NinePatchRect/NinePatchRect2/D2TR3
@onready var d2pr1 = $UI/Control/NinePatchRect/NinePatchRect2/D2PR1
@onready var d2pr2 = $UI/Control/NinePatchRect/NinePatchRect2/D2PR2
@onready var d2pr3 = $UI/Control/NinePatchRect/NinePatchRect2/D2PR3

@onready var d3cr1 = $UI/Control/NinePatchRect/NinePatchRect3/D3CR1
@onready var d3cr2 = $UI/Control/NinePatchRect/NinePatchRect3/D3CR2
@onready var d3cr3 = $UI/Control/NinePatchRect/NinePatchRect3/D3CR3
@onready var d3tr1 = $UI/Control/NinePatchRect/NinePatchRect3/D3TR1
@onready var d3tr2 = $UI/Control/NinePatchRect/NinePatchRect3/D3TR2
@onready var d3tr3 = $UI/Control/NinePatchRect/NinePatchRect3/D3TR3
@onready var d3pr1 = $UI/Control/NinePatchRect/NinePatchRect3/D3PR1
@onready var d3pr2 = $UI/Control/NinePatchRect/NinePatchRect3/D3PR2
@onready var d3pr3 = $UI/Control/NinePatchRect/NinePatchRect3/D3PR3

@onready var type1 = $UI/Control/NinePatchRect/NinePatchRect/Type1
@onready var type2 = $UI/Control/NinePatchRect/NinePatchRect2/Type2
@onready var type3 = $UI/Control/NinePatchRect/NinePatchRect3/Type3

@onready var desc1 = $UI/Control/NinePatchRect/NinePatchRect/Desc1
@onready var desc2 = $UI/Control/NinePatchRect/NinePatchRect2/Desc2
@onready var desc3 = $UI/Control/NinePatchRect/NinePatchRect3/Desc3

@onready var day = $UI/Control/NinePatchRect/NinePatchRect4/Day
@onready var delivery = $UI/Control/NinePatchRect/NinePatchRect4/Delivery
@onready var warning = $UI/Control/NinePatchRect/Warning

enum CityStates {
	NONE,
	INIT,
	START,
	STOP,
	IDLE,
	PICKING,
	PICKED,
	DRIVE,
	HOME,
}

var next_state: CityStates = CityStates.NONE
var state: CityStates = CityStates.NONE

var data1 = {}
var data2 = {}
var data3 = {}

func on_process(game_state: Game.GameStates, delta: float):
	
	match game_state:
		
		Game.GameStates.QUIT:
			print("quit")
			
	_process_city()
	state = next_state
		
func on_quitting():
	print("quit")

func _process_city():
	
	match state:
		CityStates.NONE:
			next_state = CityStates.INIT
			
		CityStates.INIT:
			_city_init()
			next_state = CityStates.START

		CityStates.START:
			next_state = CityStates.IDLE

	
func _ready():

	Game.play_music("title")
		
	Game.on_game_process.connect(on_process)
	Game.on_game_quitting.connect(on_quitting)
	
	next_state = CityStates.INIT

func _city_init():
	
	if Global.player_delivery > 4:
		Global.player_delivery = 1
		Global.player_day += 1
		
	if Global.player_day > 7:
		SceneManager.transition_to("res://game/scenes/win.tscn")
		pass
		
	day.text = "DAY %d/7" % Global.player_day
	delivery.text = "DELIVERY %d/4" % Global.player_delivery
	
	warning.text = ""
	
	if Global.player_failed > 0:
		warning.text = "** %d/3 FAILED ATTEMPTS TODAY **" % Global.player_failed
	
	_update_buckets()
			
	var traffic_bag: Array = _create_bag(Global.traffic_bucket)
	var crime_bag: Array = _create_bag(Global.crime_bucket)
	var priority_bag: Array = _create_bag(Global.priority_bucket)
	var address_bag: Array = _create_bag(Global.address_data)
	
	var traffic = _bag_pick(traffic_bag)
	var crime = _bag_pick(crime_bag)
	var priority = _bag_pick(priority_bag)
	var address = _bag_pick(address_bag)
	var miles = randi_range(10, 25)
	var type = Global.summons_types[randi_range(0, 2)]
	var type_name = _get_type_name(type)
	var desc = _get_desc(type)
	
	address1.text = address
	miles1.text = "%d MILES" % miles
	type1.text = type_name
	desc1.text = desc	
	
	d1cr2.hide()
	d1cr3.hide()
	if crime == "m":
		d1cr2.show()
	if crime == "h":
		d1cr2.show()
		d1cr3.show()
		
	d1tr2.hide()
	d1tr3.hide()
	if traffic == "m":
		d1tr2.show()
	if traffic == "h":
		d1tr2.show()
		d1tr3.show()
		
	d1pr2.hide()
	d1pr3.hide()
	if priority == "m":
		d1pr2.show()
	if priority == "h":
		d1pr2.show()
		d1pr3.show()
		
	data1 = {
		"distance": miles, # 5+
		"setting": "urb", # cit, urb
		"priority": priority, #h/m/l
		"crime": crime, # h/m/l
		"traffic": traffic, # h/m/l
		"type": type, # civ, crim, adm
		"desc": desc
	}
	
	traffic = _bag_pick(traffic_bag)
	crime = _bag_pick(crime_bag)
	priority = _bag_pick(priority_bag)
	address = _bag_pick(address_bag)
	miles = randi_range(10, 25)
	type = Global.summons_types[randi_range(0, 2)]
	type_name = _get_type_name(type)
	desc = _get_desc(type)

	address2.text = address
	miles2.text = "%d MILES" % miles	
	type2.text = type_name
	desc2.text = desc	

	d2cr2.hide()
	d2cr3.hide()
	if crime == "m":
		d2cr2.show()
	if crime == "h":
		d2cr2.show()
		d2cr3.show()
		
	d2tr2.hide()
	d2tr3.hide()
	if traffic == "m":
		d2tr2.show()
	if traffic == "h":
		d2tr2.show()
		d2tr3.show()
		
	d2pr2.hide()
	d2pr3.hide()
	if priority == "m":
		d2pr2.show()
	if priority == "h":
		d2pr2.show()
		d2pr3.show()
		
	data2 = {
		"distance": miles, # 5+
		"setting": "urb", # cit, urb
		"priority": priority, #h/m/l
		"crime": crime, # h/m/l
		"traffic": traffic, # h/m/l
		"type": type, # civ, crim, adm
		"desc": desc
	}

	traffic = _bag_pick(traffic_bag)
	crime = _bag_pick(crime_bag)
	priority = _bag_pick(priority_bag)
	address = _bag_pick(address_bag)
	miles = randi_range(10, 25)
	type = Global.summons_types[randi_range(0, 2)]
	type_name = _get_type_name(type)
	desc = _get_desc(type)

	address3.text = address
	miles3.text = "%d MILES" % miles	
	type3.text = type_name
	desc3.text = desc	

	d3cr2.hide()
	d3cr3.hide()
	if crime == "m":
		d3cr2.show()
	if crime == "h":
		d3cr2.show()
		d3cr3.show()
		
	d3tr2.hide()
	d3tr3.hide()
	if traffic == "m":
		d3tr2.show()
	if traffic == "h":
		d3tr2.show()
		d3tr3.show()
		
	d3pr2.hide()
	d3pr3.hide()
	if priority == "m":
		d3pr2.show()
	if priority == "h":
		d3pr2.show()
		d3pr3.show()
		
	data3 = {
		"distance": miles, # 5+
		"setting": "urb", # cit, urb
		"priority": priority, #h/m/l
		"crime": crime, # h/m/l
		"traffic": traffic, # h/m/l
		"type": type, # civ, crim, adm
		"desc": desc
	}
	next_state = CityStates.IDLE

func _get_type_name(type):
	
	if type == "civ":
		return "CIVIL"
		
	if type == "cri":
		return "CRIMINAL"
		
	if type == "adm":
		return "ADMIN"

func _get_desc(type):
	
	if type == "civ":
		return Global.civil_data[randi_range(0,3)].to_upper()
		
	if type == "cri":
		return Global.criminal_data[randi_range(0,3)].to_upper()
		
	if type == "adm":
		return Global.admin_data[randi_range(0,3)].to_upper()
		
func _update_buckets():

	if Global.player_day < 2:
		for i in range(4):
			Global.traffic_bucket.append("l")
			Global.crime_bucket.append("l")
			Global.priority_bucket.append("l")
	
	if Global.player_day >= 2 and Global.player_day < 4:
		for i in range(5):
			Global.traffic_bucket.append("m")
			Global.crime_bucket.append("m")
			Global.priority_bucket.append("m")
	
	if Global.player_day >= 4:
		for i in range(6):
			Global.traffic_bucket.append("h")
			Global.crime_bucket.append("h")
			Global.priority_bucket.append("h")

		
func _create_bag(data: Array):
	var bag = []
	for i in range(data.size()):
		bag.append(data[i])
	return bag
	
func _bag_pick(data: Array):
	data.shuffle()
	var index = randi_range(0, data.size()-1)
	var pick = data[index]
	data.remove_at(index)
	return pick
	
func _on_city_1_pressed():
	
	Global.set_driving_data(data1)
	print(Global.driving_data)
	SceneManager.transition_to("res://game/scenes/driving.tscn")

func _on_city_2_pressed():
	
	Global.set_driving_data(data2)
	print(Global.driving_data)
	SceneManager.transition_to("res://game/scenes/driving.tscn")

func _on_city_3_pressed():

	Global.set_driving_data(data3)
	print(Global.driving_data)
	SceneManager.transition_to("res://game/scenes/driving.tscn")
