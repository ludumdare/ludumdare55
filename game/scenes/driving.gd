extends Node2D

@export var street_entity: PackedScene
@export var streets: Array[PackedScene] = []
@export var buildings: Array[PackedScene] = []
@export var radar_scene: PackedScene
@export var police_scene: PackedScene
@export var car_scene: PackedScene
@export var completed_scene: PackedScene
@export var failed_scene: PackedScene

@onready var root = $Entities/StreetRoot
@onready var entities = $Entities
@onready var camera = $Entities/PlayerEntity/CameraEntity
@onready var debug = $UI/Debug
@onready var timer1 = $Timer1 # collided timer
@onready var timer2 = $Timer2 # finished timer
@onready var timer3 = $Timer3 # no_collision timer
@onready var ui = $UI
@onready var speedbar = $UI/Control/SpeedProgress
@onready var speedlabel = $UI/Control/Speed

enum DriverStates {
	NONE,
	INIT,
	START,
	STOP,
	IDLE,
	FINISHED,	
	COLLIDED,
	RUN,
	HOME,
	NEXT,
	FAILED,
	LOSE
}

var next_state: DriverStates = DriverStates.NONE
var state: DriverStates = DriverStates.NONE

var driver_streets: Array = []
var current_street: int = 0
var total_streets: int = 0
var speed_limit: int = 50
var elapsed_time: float = 0.0
var time_remaining: float = 0.0
var time_allocated: float = 0.0
var player_chased: bool = false
var no_collision: bool = false
var cops_following: Array = []
var top_speed: int = 0
var build_info = "\n Build Info:\n"
var total_distance: int = 0
var driver_priority: int = 0

var siren: AudioStreamPlayer

func on_process(game_state: Game.GameStates, delta: float):

	_process_driver(delta)
	state = next_state
	
	var debug_data = "\n\n Running:\n"
	debug_data += "  GAME STATE:   %s\n" % game_state
	debug_data += "  STATE:        %s\n" % state
	debug_data += "  NEXT STATE:   %s\n" % next_state
	debug_data += "  STREET:       %s\n" % current_street
	debug_data += "  SPEED:        %s\n" % Global.player_speed
	debug_data += "  SIM_SPEED:    %d\n" % (Global.player_speed / 6)
	debug_data += "  TIME_EST:     %2.1f\n" % time_allocated
	debug_data += "  TIME_ELAP:    %2.1f\n" % elapsed_time
	debug_data += "  TIME_REM:     %2.1f\n" % time_remaining
	debug_data += "  CHASED:       %s\n" % player_chased
	debug_data += "  COLLISIONS:   %s\n" % !no_collision
	debug_data += build_info
	
	debug.text = debug_data

	elapsed_time += delta
	time_remaining = time_allocated - elapsed_time
	
	if Input.is_action_just_pressed("game_debug"):
		debug.visible = !debug.visible
		
	speedbar.value = Global.player_speed
	speedlabel.text = "SPEED %d mph" % round(Global.player_speed/6)
	speedbar.tint_progress = Color.GREEN
	#if Global.player_speed > 200:
		#speedbar.tint_progress = Color.YELLOW
	if Global.player_speed > 300:
		speedbar.tint_progress = Color.RED
		
	if Global.player_speed > top_speed:
		top_speed = Global.player_speed
		
	$UI/Control/Money.text = "MONEY: $%d" % Global.player_money
	$UI/Control/XP.text = "XP: %d" % Global.player_xp
	
	$UI/Control/Timer.text = "%3.1f" % time_remaining
		
func on_quitting():
	print("quit")
	

func _process_driver(delta):
	
	match state:
		
		DriverStates.NONE:
			Logger.trace("driver state - none")
			next_state = DriverStates.INIT
			
		DriverStates.INIT:
			Logger.trace("driver state - init")
			_do_init()
			next_state = DriverStates.START
			
		DriverStates.START:
			Logger.trace("driver state - start")
			elapsed_time = 0
			Game.play_music("driving")
			next_state = DriverStates.RUN

		DriverStates.RUN:
			Logger.trace("driver state - run")
			ECS.update("run")
			ECS.update("police")
			if time_remaining <= 0:
				next_state = DriverStates.FAILED
				Game.play_music("complete")
				Global.player_failed += 1
				if Global.player_failed > 3:
					ECS.clean()
					ECS.update
					next_state = DriverStates.LOSE
			
		DriverStates.FINISHED:
			Logger.trace("driver state - run")
			if siren != null:
				siren.stop()
			no_collision = true
			ECS.update("collided")
			next_state = DriverStates.IDLE
			Global.player_delivery += 1
			Global.time_bonus = round(time_allocated - time_remaining) * 5
			Global.top_speed = top_speed
			Global.xp_earned = total_distance + (Global.time_bonus * 2) 
			Global.money_earned = (100 * driver_priority)
			Global.player_money += Global.time_bonus + Global.money_earned
			Global.player_xp += Global.xp_earned
			var win = completed_scene.instantiate()
			ui.add_child(win)
			timer2.start()
			
		DriverStates.FAILED:
			Logger.trace("driver state - failed")
			if siren != null:
				siren.stop()
			no_collision = true
			ECS.update("collided")
			var failed = failed_scene.instantiate()
			ui.add_child(failed)
			timer2.start()
			next_state = DriverStates.IDLE
			
				
		DriverStates.COLLIDED:
			Logger.trace("driver state - collided")
			no_collision = true
			ECS.update("collided")
			ECS.update("police")
				
		DriverStates.STOP:
			Logger.trace("driver state - stop")
			ECS.clean()
			ECS.update
			next_state = DriverStates.NEXT
				
		DriverStates.NEXT:
			Logger.trace("driver state - next")
			ECS.update()
			SceneManager.transition_to("res://game/scenes/city.tscn")
			
		DriverStates.LOSE:
			Logger.trace("driver state - lose")
			ECS.update()
			SceneManager.transition_to("res://game/scenes/lose.tscn")
				
	state = next_state
	
	
func _do_init():
	
	print(Global.driving_data)
	
	var type = Global.driving_data["type"]
	var distance = Global.driving_data["distance"]
	var priority = Global.driving_data["priority"]
	var crime = Global.driving_data["crime"]
	var traffic = Global.driving_data["traffic"]
	
	total_distance = distance
	
	driver_priority = 1
	
	if priority == "m":
		driver_priority = 2
	if priority == "h":
		driver_priority = 3
	
	var time_factor = 2.15 # x seconds for every street
	var cop_factor = 5.25 # 1 cop in every x streets
	var traffic_factor = 1.2 # x cars for every 1 streets
	
	if priority == "h":
		time_factor -= 1
		
	if priority == "l":
		time_factor += 1
		
	if crime == "h":
		cop_factor += 1
		
	if crime == "l":
		cop_factor -= 1
		
	if traffic == "l":
		traffic_factor -= 1
		
	if traffic == "h":
		traffic_factor += 1
		
	total_streets = round(distance * 2.25)
	time_allocated = total_streets * time_factor # seconds for each street
	
	var cops = roundi(total_streets / cop_factor) + 1
	var cars = round(total_streets * traffic_factor)
	
	for i in range(total_streets + 3):
		
		var street = {
			"pos": Vector2i.RIGHT * (i * 512),
			"scene": 0,
			"cars": 1
		}
		
		driver_streets.append(street)
		
	var pick_bag = []
	for i in range(5, total_streets):
		pick_bag.append(i)
		
	for i in range(cops):
		pick_bag.shuffle()
		var loc = randi_range(0, pick_bag.size()-1)
		_add_cop(pick_bag[loc])
		pick_bag.remove_at(pick_bag[loc])
	
	pick_bag = []
	for i in range(1, total_streets):
		pick_bag.append(i)
		
	var car_bag = {}
	
	for i in range(cars):
		pick_bag.shuffle()
		var loc = randi_range(3, pick_bag.size()-1)
		
		if car_bag.has(pick_bag[loc]):
			car_bag[pick_bag[loc]] += 1
		else:
			car_bag[pick_bag[loc]] = 1
			
	for i in pick_bag.size():
		if car_bag.has(i):
			var total_cars = car_bag[i]
			var street = driver_streets[i]
			street.cars = total_cars
		
	speed_limit = 50
	
	_add_street()
	
	build_info += "  TYPE:        %s\n" % type
	build_info += "  DIST:        %s\n" % distance
	build_info += "  PRIORITY:    %s\n" % priority
	build_info += "  CRIME:       %s\n" % crime
	build_info += "  STREETS:     %s\n" % total_streets
	build_info += "  CARS:        %s\n" % cars	
	build_info += "  COPS:        %s\n" % cops	
	build_info += "  SPEED LIMIT: %s\n" % speed_limit
	
func _ready():
	
	#var child = find_child("PlayerEntity",true, false)
	#print(child)
	#camera.position = Vector2.ZERO
	#camera.reparent(child)
	
	Global.player_speed = 50
	
	Game.on_game_process.connect(on_process)
	Game.on_game_quitting.connect(on_quitting)
	Global.street_end.connect(_on_street_end)
	Global.police_radar.connect(_on_police_radar)
	Global.caught_speeding.connect(_on_caught_speeding)
	Global.car_collision.connect(_on_car_collision)
	

func _on_caught_speeding():
	print("caught!")

func _on_car_collision():
	
	if no_collision:
		return
		
	print("collided")
	timer1.start()
	Global.player_speed = 0
	next_state = DriverStates.COLLIDED
	
func _on_police_radar():
	
	if player_chased || round(Global.player_speed / 6) > speed_limit:
		var police = police_scene.instantiate()
		cops_following.append(police)
		call_deferred("add_child", police)
		police.position = Global.player_entity.position + Vector2.LEFT * 1000
		if siren == null:
			siren = Game.play_and_return_sound("siren")


func _on_street_end():

	current_street += 1
	
	# at the last street -- start finish sequence
		
	if current_street > total_streets:
		Game.play_music("complete")
		next_state = DriverStates.FINISHED

	if current_street > driver_streets.size() -1:
		next_state = DriverStates.IDLE
		return
			
	_add_street()
	
	
func _add_street():
	Logger.debug("[Driver] _add_street")
	var ent = street_entity.instantiate()
	var street = streets[driver_streets[current_street].scene].instantiate()
	for i in range(3):
		var index = randi_range(0, buildings.size()-1)
		var building = buildings[index].instantiate()
		street.add_child(building)
		building.position = Vector2.RIGHT * (i * 176)
		building.position += Vector2.UP * 80
	ent.add_child(street)
	ent.position = driver_streets[current_street].pos
	root.call_deferred("add_child", ent)
	var cars = driver_streets[current_street].cars
	Logger.debug("cars %s" % cars)
	for i in range(cars):
		#if i == 0:
			#_add_car_player(current_street)
		_add_car(current_street)
	
	
	

func _add_cop(location):
	Logger.debug("[Driver] _add_cop")
	var cop = radar_scene.instantiate()
	cop.position = (Vector2.RIGHT * (location * 960)) + (Vector2.DOWN * 512)
	entities.call_deferred("add_child", cop)
	
	
func _add_car(location):
	Logger.debug("[Driver] _add_car")
	var car = car_scene.instantiate()
	#root.add_child(car)
	entities.call_deferred("add_child", car)
	var xpos = randi_range(0,4)
	car.position = Vector2(Global.player_entity.position.x, 512) + Vector2.RIGHT * (1024 + (xpos * 512))
	var ypos = randi_range(1,8)
	car.position += Vector2.UP * (ypos * 32)
	
func _add_car_player(location):
	return
	Logger.debug("[Driver] _add_car_player")
	var car = car_scene.instantiate()
	entities.add_child(car)
	#root.call_deferred("add_child", car)
	var xpos = randi_range(-4,4)
	car.position = Vector2(Global.player_entity.position.x, 0) + Vector2.RIGHT * (2048 + xpos * 128)
	var ypos = Global.player_entity.global_position.y
	car.global_position = Vector2(car.global_position.x, ypos)
	
	
func _init():
	pass
	

func _on_timer_1_timeout():
	no_collision = true
	timer3.start()
	next_state = DriverStates.RUN

func _on_timer_2_timeout():
	next_state = DriverStates.STOP

func _on_timer_3_timeout():
	no_collision = false;
