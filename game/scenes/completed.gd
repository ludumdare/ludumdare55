extends Control

func _ready():
	
	var stats = ""
	
	stats += "Money Earned: %d\n" % Global.money_earned
	stats += "Time Bonus: %d\n" % Global.time_bonus
	stats += "XP Earned: %d\n" % Global.xp_earned
	stats += "Top Speed: %d\n" % round(Global.top_speed / 6)

	$Stats.text = stats
